#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// pow(a, b) ==> exp(log(a) * b)

int test1(int i) {
  return i * 2;
}

int test2(int i) {
  return i * 4;
}

int test3(int i) {
  return i * 5;
}

double test4(int i) {
  return pow(2, i);
}

double test5(int i) {
  return (double)(1 << i);
}

double test6(int i) {
  return pow(5, i);
};

double test7(int i) {
  return pow(-1, i);
}

int test8(int i) {
  return i % 2 == 0 ? 1 : -1;
};

double test9(int i) {
  return i % 2 == 0 ? 1 : -1;
}

double test10(int i) {
  int sign = i % 2 == 0 ? 1 : -1;
  return (double)sign;
}

double test11(int i, int k) {
  return pow(i, k);
}


int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf("Usage\n%s <number>\n", argv[0]);
    return 1;
  }
  int i = atoi(argv[1]);

//printf( "Enter a value: ");
//  scanf("%d", &i);

  printf("input=%d ", i);
//  printf("test1=%d ", test1(i));
//  printf("test2=%d ", test2(i));
// printf("test3=%f ", test3(i));
  printf("test4=%f ", test4(i));
  printf("test5=%f ", test5(i));
  printf("\n");
  return 0;
}
