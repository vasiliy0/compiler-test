#!/bin/bash
set -e
# libc6-dev-i386
#for i in `seq -5 5`; do
# ./1.bin $i
#one
for i in `seq 1 11`; do
  echo -e "\n\n\n===== Function test$i ====="
  for o in `seq 0 3`; do
    echo -e "\nOptimization level $o"
    gcc -m32 1.c -o 1.bin -g -lm -O$o
    if [ "$o" == "0" ]; then
      gdb -q 1.bin -ex "set listsize 5" -ex "list test$i" --batch
    fi
    gdb -q 1.bin -ex "disassemble test$i" --batch
  done
done

echo -e "\n\nTest run\n"
for i in `seq -5 5`; do
  ./1.bin $i
done
